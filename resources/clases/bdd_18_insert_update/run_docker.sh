docker run \
 -it \
 --network host \
 --name docker-postgres \
 --volume $(pwd)
 -e POSTGRES_PASSWORD=mysecretpassword \
 -d postgres

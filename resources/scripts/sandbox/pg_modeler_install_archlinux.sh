# Install PostgreSQL 
# Create the file repository configuration

VERSION=1.0.2

echo "Actualizando sistema..."
sleep 3
sudo pacman -Syu
sudo pacman -S git

echo "Instalando requerimientos..."
sleep 3
sudo pacman -S build-essential libxml2-dev # Install Qt
sudo pacman -S qtcreator qt6-default libpq-dev libqt6svg5-dev # Set up our local environmenti
paru -Sy qtchooser
export QT_ROOT=/usr/lib/qt6
export INSTALLATION_ROOT=~/.sw/pgmodeler/bin/$VERSION
echo "Creando directorio INSTALLATION_ROOT: $INSTALLATION_ROOT"
mkdir -v -p $INSTALLATION_ROOT # Get the pgModeler repo
echo "Cambiando directorio a ~/.sw/pgmodeler/bin/$VERSION"
cd ~/.sw/pgmodeler/bin/$VERSION
echo "Descargando proyecto..."
git clone https://github.com/pgmodeler/pgmodeler.git
cd pgmodeler
git pull
echo "Seleccionando al Version: $VERSION..."
git checkout v$VERSION # Configure the build
# cd ~/.sw/pgmodeler/bin/0.9.4
echo "Iniciando Compilacion..."
sleep 3
qmake -r CONFIG+=release \
PREFIX=$INSTALLATION_ROOT \
BINDIR=$INSTALLATION_ROOT \
PRIVATEBINDIR=$INSTALLATION_ROOT \
PRIVATELIBDIR=$INSTALLATION_ROOT/lib pgmodeler.pro # Build pgModeler
# cd ~/.sw/pgmodeler/bin/0.9.4
make
echo "Instalando Version $VERSION"
make install
echo "PGModeler $VERSION Instalado :)"

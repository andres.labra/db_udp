# Install PostgreSQL 
# Create the file repository configuration

export QT_ROOT=/usr/lib/qt5
export INSTALLATION_ROOT=~/.sw/pgmodeler/bin/0.9.4
mkdir -p $INSTALLATION_ROOT # Get the pgModeler repo
cd ~/.sw/pgmodeler/bin/0.9.4
git clone https://github.com/pgmodeler/pgmodeler.git
mv pgmodeler src
cd src
git pull
git checkout v0.9.4 # Configure the build
# cd ~/.sw/pgmodeler/bin/0.9.4
qmake -r CONFIG+=release \
PREFIX=$INSTALLATION_ROOT \
BINDIR=$INSTALLATION_ROOT \
PRIVATEBINDIR=$INSTALLATION_ROOT \
PRIVATELIBDIR=$INSTALLATION_ROOT/lib pgmodeler.pro # Build pgModeler
# cd ~/.sw/pgmodeler/bin/0.9.4
echo "PREPARING MAKE..."
sleep 10
make
echo "PREPARING MAKE INSTALL"
sleep 10
sudo make install
echo "PGModeler Instalado :)"
